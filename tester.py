def on_button_pressed_a():
    music.play_tone(175, music.beat(BeatFraction.QUARTER))
    if adjMode == "SPEED":
        # dec speed
        radio.raise_event(4900, 4901)
    elif adjMode == "RATIO":
        # dec ratio
        radio.raise_event(4900, 4903)
    else:
        # dec thres
        radio.raise_event(4900, 4905)
input.on_button_pressed(Button.A, on_button_pressed_a)

# toggle PAUSE/RUN
def on_button_pressed_ab():
    music.play_tone(262, music.beat(BeatFraction.HALF))
    music.play_tone(294, music.beat(BeatFraction.HALF))
    music.play_tone(330, music.beat(BeatFraction.HALF))
    # inc ratio
    radio.raise_event(4900, 4907)
input.on_button_pressed(Button.AB, on_button_pressed_ab)

def on_received_string(receivedString):
    if receivedString == "CLEAR":
        OLED.clear()
    else:
        OLED.write_string_new_line(receivedString)
radio.on_received_string(on_received_string)

def on_button_pressed_b():
    music.play_tone(698, music.beat(BeatFraction.QUARTER))
    if adjMode == "SPEED":
        # inc speed
        radio.raise_event(4900, 4902)
    elif adjMode == "RATIO":
        # dec ratio
        radio.raise_event(4900, 4904)
    else:
        # inc thres
        radio.raise_event(4900, 4906)
input.on_button_pressed(Button.B, on_button_pressed_b)

def on_logo_pressed():
    global adjMode
    music.play_tone(349, music.beat(BeatFraction.QUARTER))
    if adjMode == "SPEED":
        basic.show_string("R")
        adjMode = "RATIO"
    elif adjMode == "RATIO":
        basic.show_string("T")
        adjMode = "THRES"
    else:
        basic.show_string("S")
        adjMode = "SPEED"
input.on_logo_event(TouchButtonEvent.PRESSED, on_logo_pressed)

adjMode = ""
led.set_brightness(180)
OLED.init(128, 64)
OLED.write_string_new_line("track car tester")
OLED.write_string_new_line("V3.1")
OLED.write_string_new_line("2022 11 24")
basic.pause(3000)
radio.set_group(180)
adjMode = "SPEED"
basic.show_string("S")